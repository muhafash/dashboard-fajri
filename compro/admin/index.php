<?php
$name= "Muhammad Fajri Assidiq";
$mentor= "Kak Doni";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body class="body">
<div class="container">
    <nav class="navbar">
        <div class="navbar">
            <h4><?php echo $name; ?></h4>
            <a href="#"><?php echo"Home"; ?></a>
        </div>
    </nav>

    <!-- main -->
    <main class="main">
        <div class="main-container">
            <div class="main-greet">
                <h3>Hallo Selamat Datang <?php echo $mentor?></h3>
            </div>
        </div>
    </main>

    <!-- SideBar -->
    <div class="sidebar">
        <div class="sidebar-title">
            <h3>Tech Store</h3>
        </div>
    <div class="sidebar-menu">
        <div class="sidebar-link active-sidebar-link" >
            <a href="#">Dashboard</a>
        </div>
        <div class="sidebar-link">
            <a href="#">Product Content</a>
        </div>
    </div>
    </div>
</div>
    
</body>
</html>
